# Polytope_qp_ws

## Instalation 

```bash
git clone --recurse-submodules https://gitlab.inria.fr/auctus-team/people/lucas-joseph/polytope_qp_ws.git
cd polytope_qp_ws
catkin config --init --extend=/opt/ros/noetic --cmake-args -DCMAKE_BUILD_TYPE=Release
catkin build
```

## Usage

## Simulation

```bash
roslaunch polytope_qp_control run.launch sim:=true
```

## Real robot 

```bash
roslaunch polytope_qp_control run.launch sim:=false
```

## Launch a trajectory

```bash
rosrun traj_gen_ros test.py
```
